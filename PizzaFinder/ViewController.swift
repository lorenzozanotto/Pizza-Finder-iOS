//
//  ViewController.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var spotifyParser = SpotifyParser()
    var playlistArray = [PizzaPlaylist]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureDataModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlistArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "PizzaCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PlaylistCell
        
        let cellElement = playlistArray[indexPath.row]
        
        cell.configureCell(cellElement.title, uri: cellElement.uri)
        
        return cell        
    }
    
    // MARK: - Data Model Initializers
    func configureDataModel() {
        spotifyParser.executeSpotifyRequest({
            self.retrieveDecodedPlaylists()
            self.tableView.reloadData()
        })
    }
    
    // MARK: - Data retrieving methods
    func retrieveDecodedPlaylists() {
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let decoded  = userDefaults.objectForKey("playlists") as! NSData
        let decodedTeams = NSKeyedUnarchiver.unarchiveObjectWithData(decoded) as! [PizzaPlaylist]
        playlistArray = decodedTeams
        
    }
    
    // MARK: - Segue and Data Transmission Configuration
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "displaySongsSegue") {
            if let indexPath = tableView.indexPathForSelectedRow {
                if let destinationController = segue.destinationViewController as? SongsViewController {
                    destinationController.selectedPlaylistUri = playlistArray[indexPath.row].uri
                    destinationController.selectedPlaylistOwner = playlistArray[indexPath.row].owner
                }
            }
            
        }
    }

}

