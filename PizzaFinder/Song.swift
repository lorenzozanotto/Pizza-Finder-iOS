//
//  Song.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation

class Song: NSObject, NSCoding {
    
    var title: String
    var uri: String
    
    init(title: String, uri: String) {
        self.title = title
        self.uri = uri
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObjectForKey("title") as! String
        let uri = aDecoder.decodeObjectForKey("uri") as! String
        self.init(title: title, uri: uri)
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(title, forKey: "title")
        aCoder.encodeObject(uri, forKey: "uri")
    }
    
}