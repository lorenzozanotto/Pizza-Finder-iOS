//
//  PlaylistCell.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import UIKit

class PlaylistCell: UITableViewCell {
    
    @IBOutlet weak var playlistTitleLabel: UILabel!
    @IBOutlet weak var playlistUriLabel: UILabel!
    
    // MARK: - Cell View Configuration
    func configureCell(title: String, uri: String) {
        self.playlistTitleLabel.text = title
        self.playlistUriLabel.text = uri
    }
    

}
