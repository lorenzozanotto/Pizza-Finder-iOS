//
//  SpotifyParser.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SpotifyParser: NSObject {
    
    
    // Uri del documento da parsare
    let parseUri = "http://lorenzozanotto.com/emmental/spotify.json"
    var playlistItems = [PizzaPlaylist]()
    
    let headers = [
        "Authorization": authToken,
        "Content-Type": "application/json"
    ]
    
    func executeSpotifyRequest(completed: FetchCompleted) {
        
        Alamofire.request(.GET, "https://api.spotify.com/v1/me/playlists", headers: headers).responseJSON { response in
            
            self.parseJson(response.data!)
            
            // Serializing and saving data on NSUserDefaults
            self.serializeAndSaveData(self.playlistItems)
            
            // Execute completion bolck
            completed()
            
        }
        
    }
    
    func parseJson(JSONFile: NSData) {
     
        let json = JSON(data: JSONFile)
        var nPlaylists = 0
        
        if let numberOfPlaylists = json["total"].int {
            nPlaylists = numberOfPlaylists
        }
        
        // Retrieving Playlist's name
        for i in 0...nPlaylists {
            if let playlistName = json["items"][i]["name"].string {
                if let playlistUri = json["items"][i]["id"].string {
                    if let playlistOwner = json["items"][i]["owner"]["id"].string {
                        let playlistObject = PizzaPlaylist(title: playlistName, uri: playlistUri, owner: playlistOwner)
                        self.playlistItems.append(playlistObject)
                        print("Playlist name \(playlistName)")
                        print("Playlist owner \(playlistOwner)")
                    }
                    
                }
                
                
            }
        }
        
    }
    
    func serializeAndSaveData(playlistArray: [PizzaPlaylist]) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(playlistArray)
        userDefaults.setObject(encodedData, forKey: "playlists")
        userDefaults.synchronize()
    }

}
