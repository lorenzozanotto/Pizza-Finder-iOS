//
//  SongsViewController.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import UIKit

class SongsViewController: UITableViewController {
    
    var spotifySongParser = SpotifyTrackParser()
    var songsArray = [Song]()
    var selectedPlaylistUri: String!
    var selectedPlaylistOwner: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureDataModel()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songsArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        cell.textLabel?.text = songsArray[indexPath.row].title
        
        return cell
    }
    
    // MARK: - Data Model Initializers
    func configureDataModel() {
        spotifySongParser.executeSpotifyRequest(selectedPlaylistUri, playlistOwner: selectedPlaylistOwner, completed: {
            self.retrieveDecodedPlaylists()
            self.tableView.reloadData()
        })
        retrieveDecodedPlaylists()
    }
    
    // MARK: - Data retrieving methods
    func retrieveDecodedPlaylists() {
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if (userDefaults.objectForKey("songs") != nil) {
            let decoded =  userDefaults.objectForKey("songs") as! NSData
            let decodedElements = NSKeyedUnarchiver.unarchiveObjectWithData(decoded) as! [Song]
            songsArray = decodedElements
        }
        
    }
    

}
