//
//  SpotifyTrackParser.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SpotifyTrackParser {
    
    // Uri del documento da parsare
    let parseUri = "http://lorenzozanotto.com/emmental/spotify-songs.json"
    var songs = [Song]()
    
    let headers = [
        "Authorization": authToken,
        "Content-Type": "application/json"
    ]
    
    func executeSpotifyRequest(playlistID: String, playlistOwner: String, completed: FetchCompleted) {
        
        Alamofire.request(.GET, "https://api.spotify.com/v1/users/\(playlistOwner)/playlists/\(playlistID)/tracks", headers: headers).responseJSON { response in
            
            self.parseJson(response.data!)
            print("Il numero di elementi è \(self.songs.count)")
            
            // Serializing and saving data on NSUserDefaults
            self.serializeAndSaveData(self.songs)
            
            // Executing notification block
            completed()
        }
        
    }
    
    func parseJson(JSONFile: NSData) {
        
        let json = JSON(data: JSONFile)
        var nPlaylists = 0
        
        print("FUNZIONA")
        
        if let numberOfPlaylists = json["total"].int {
            nPlaylists = numberOfPlaylists
        }
        
        // Retrieving Playlist's name
        for i in 0...nPlaylists {
            if let songName = json["items"][i]["track"]["name"].string {
                if let songUri = json["items"][i]["track"]["uri"].string {
                    let songObject = Song(title: songName, uri: songUri)
                    print(songObject.title)
                    print(songObject.uri)
                    self.songs.append(songObject)
                }
                
            }
        }
        
    }
    
    func serializeAndSaveData(songsArray: [Song]) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        let encodedData = NSKeyedArchiver.archivedDataWithRootObject(songsArray)
        userDefaults.setObject(encodedData, forKey: "songs")
        userDefaults.synchronize()
        print("Dati salvati")
    }
    
}
