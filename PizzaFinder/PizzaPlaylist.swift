//
//  PizzaPlaylist.swift
//  PizzaFinder
//
//  Created by Lorenzo Zanotto on 27/06/2016.
//  Copyright © 2016 Lorenzo Zanotto. All rights reserved.
//

import Foundation

class PizzaPlaylist: NSObject, NSCoding {
    
    var title: String
    var uri: String
    var owner: String
    
    init(title: String, uri: String, owner: String) {
        self.title = title
        self.uri = uri
        self.owner = owner
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        let title = aDecoder.decodeObjectForKey("title") as! String
        let uri = aDecoder.decodeObjectForKey("uri") as! String
        let owner = aDecoder.decodeObjectForKey("owner") as! String
        self.init(title: title, uri: uri, owner: owner)
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(title, forKey: "title")
        aCoder.encodeObject(uri, forKey: "uri")
        aCoder.encodeObject(owner, forKey: "owner")
    }

}
